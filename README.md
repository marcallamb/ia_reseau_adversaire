# ia_reseau_adversaire

Prérequis:
installer python3
installer keras
installer tensorflow et tensorboard


lancer le fichier tp.py avec la commande:
python3 tp.py

lancer le tensorboard avec la commande:
tensorboard --logdir logs

observer les résultats depuis un navigateur avec l'url:
localhost:6006


Nous avons remarqué durant nos tests que notre réseau arrivait à produire un système Alice-Bob qui arrivait à progresser jusqu'à se stabiliser de façon satisfaisante, mais arrivait aussi à rendre le "message" difficile à déchiffer pour Eve, ce qui nous semble appréciable. 
