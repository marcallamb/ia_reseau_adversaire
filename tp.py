#tp.py

import numpy as np
import tensorflow as tf
from datetime import datetime
import random
import string

from keras import backend as K
from keras.models import Model
from keras.engine.input_layer import Input
from keras.layers.core import Activation, Dense
from keras.layers import Flatten, Reshape
from keras.layers.convolutional import Conv1D
from keras.layers.merge import concatenate
from keras.optimizers import Adam, RMSprop
from keras.callbacks import TensorBoard




#fonction d'écriture des logs
def write_log(callback, names, logs, batch_no):
    for name, value in zip(names, logs):
        summary = tf.summary()
        print(summary)
        summary_value = summary.Value.add()
        summary_value.simple_value = value
        summary_value.tag = name
        callback.writer.add_summary(summary, batch_no)
        callback.writer.flush()


def randomString(stringLength=10):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))



# Initialise les paramètres crypto: longueur en bits des message, clé, et ciphertext
m_bits = 16
k_bits = 16
c_bits = 16
pad = 'same'

# Calcul la taille du message (utilisé plus tard dans l'entrainement)
m_train = 2**(m_bits) #+ k_bits)




#On définit le réseau de neuronne pour Alice
ainput0 = Input(shape=(m_bits,)) #message
ainput1 = Input(shape=(k_bits,)) #clé
ainput = concatenate([ainput0, ainput1], axis=1)

adense1 = Dense(units=(m_bits + k_bits))(ainput)
adense1a = Activation('tanh')(adense1)
areshape = Reshape((m_bits + k_bits, 1,))(adense1a)

aconv1 = Conv1D(filters=2, kernel_size=4, strides=1, padding=pad)(areshape)
aconv1a = Activation('tanh')(aconv1)
aconv2 = Conv1D(filters=4, kernel_size=2, strides=2, padding=pad)(aconv1a)
aconv2a = Activation('tanh')(aconv2)
aconv3 = Conv1D(filters=4, kernel_size=1, strides=1, padding=pad)(aconv2a)
aconv3a = Activation('tanh')(aconv3)
aconv4 = Conv1D(filters=1, kernel_size=1, strides=1, padding=pad)(aconv3a)
aconv4a = Activation('sigmoid')(aconv4)

aoutput = Flatten()(aconv4a)

alice = Model([ainput0, ainput1], aoutput, name='alice')





#On définit le réseau de neuronne pour Bob
binput0 = Input(shape=(c_bits,)) #cypher text
binput1 = Input(shape=(k_bits,)) #clé
binput = concatenate([binput0, binput1], axis=1)

bdense1 = Dense(units=(m_bits + k_bits))(binput)
bdense1a = Activation('tanh')(bdense1)
breshape = Reshape((m_bits + k_bits, 1,))(bdense1a)

bconv1 = Conv1D(filters=2, kernel_size=4, strides=1, padding=pad)(breshape)
bconv1a = Activation('tanh')(bconv1)
bconv2 = Conv1D(filters=4, kernel_size=2, strides=2, padding=pad)(bconv1a)
bconv2a = Activation('tanh')(bconv2)
bconv3 = Conv1D(filters=4, kernel_size=1, strides=1, padding=pad)(bconv2a)
bconv3a = Activation('tanh')(bconv3)
bconv4 = Conv1D(filters=1, kernel_size=1, strides=1, padding=pad)(bconv3a)
bconv4a = Activation('sigmoid')(bconv4)

boutput = Flatten()(bconv4a)

bob = Model([binput0, binput1], boutput, name='bob')





#On définit le réseau de neuronne pour Eve
einput = Input(shape=(c_bits,)) #juste le ciphertext

edense1 = Dense(units=(c_bits + k_bits))(einput)
edense1a = Activation('tanh')(edense1)
edense2 = Dense(units=(c_bits + k_bits))(edense1a)
edense2a = Activation('tanh')(edense2)
ereshape = Reshape((c_bits + k_bits, 1,))(edense2a)

econv1 = Conv1D(filters=2, kernel_size=4, strides=1, padding=pad)(ereshape)
econv1a = Activation('tanh')(econv1)
econv2 = Conv1D(filters=4, kernel_size=2, strides=2, padding=pad)(econv1a)
econv2a = Activation('tanh')(econv2)
econv3 = Conv1D(filters=4, kernel_size=1, strides=1, padding=pad)(econv2a)
econv3a = Activation('tanh')(econv3)
econv4 = Conv1D(filters=1, kernel_size=1, strides=1, padding=pad)(econv3a)
econv4a = Activation('sigmoid')(econv4)

eoutput = Flatten()(econv4a)# Résultat obtenu par Eve

eve = Model(einput, eoutput, name='eve')




#Paramètre d'entré des modèles
aliceout = alice([ainput0, ainput1])
bobout = bob( [aliceout, binput1] )# bob a le ciphertext et la clé
eveout = eve( aliceout )# eve n'a que le ciphertext

#Fonction de calcul des pertes
eveloss = K.mean(  K.sum(K.abs(ainput0 - eveout), axis=-1)  )

bobloss = K.mean(  K.sum(K.abs(ainput0 - bobout), axis=-1)  )

K.square(m_bits/2 - eveloss)/( (m_bits//2)**2 )

abeloss = bobloss + K.square(m_bits/2 - eveloss)/( (m_bits//2)**2 )

abeoptim = RMSprop(lr=0.001)
eveoptim = RMSprop(lr=0.001) #default 0.001





# Build et compile le model ABE utilisé pour entrainer le model Alice-Bob
#
abemodel = Model([ainput0, ainput1, binput1], bobout, name='abemodel')
abemodel.add_loss(abeloss)
abemodel.compile(optimizer=abeoptim)
abemodel.summary()

# Build et compile le model Eve utilisé pour entrainer Eve sans Alice
#
alice.trainable = False
evemodel = Model([ainput0, ainput1], eveout, name='evemodel')
evemodel.add_loss(eveloss)
evemodel.compile(optimizer=eveoptim)
evemodel.summary()



#nom de dossier pour les logs de cet entrainement
random_String = randomString()

# initialisation des logs
summary_writer = tf.summary.create_file_writer('logs/'+random_String)

tf.summary.trace_on(graph=True, profiler=True)

print("date time : " + datetime.now().strftime("%d_%m_%Y__%H_%M_%S"))


#on définit les paramètres d'entrainement
n_epochs = 80
batch_size = 512
n_batches = m_train // batch_size
print("n_batches : "+str(n_batches))
abecycles = 1
evecycles = 1

epoch = 0
while epoch < n_epochs:
    print("epoch : "+str(epoch))
    for iteration in range(n_batches):
        #print("iteration n_batches : "+str(iteration))
        # Entraine le réseau A-B+E
        #
        alice.trainable = True


        for cycle in range(abecycles):
            # On génère les messages et clé de manière aléatoire
            #
            m_batch = np.random.randint(0, 2, m_bits * batch_size).reshape(batch_size, m_bits)
            k_batch = np.random.randint(0, 2, k_bits * batch_size).reshape(batch_size, k_bits)
            abe_loss = abemodel.train_on_batch([m_batch, k_batch, k_batch], None)


        # Entrainement du réseau eve
        #
        alice.trainable = False

        for cycle in range(evecycles):
            m_batch = np.random.randint(0, 2, m_bits * batch_size).reshape(batch_size, m_bits)
            k_batch = np.random.randint(0, 2, k_bits * batch_size).reshape(batch_size, k_bits)
            eve_loss = evemodel.train_on_batch([m_batch, k_batch], None)

        #on ecrit les résultats dans les logs
        with summary_writer.as_default():
            tf.summary.scalar('loss/abe/', abe_loss, step=epoch*n_batches+iteration)
            tf.summary.scalar('loss/eve/', eve_loss, step=epoch*n_batches+iteration)




    epoch += 1
